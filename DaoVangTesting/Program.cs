﻿using System;

namespace EX_ThuThapQuang
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            try
            {
                Console.Write("Nhập số mảnh quặng đã thu thập: ");
                int soMangQuang = int.Parse(Console.ReadLine());

                int xuDongVang = TinhXuDongVang(soMangQuang);

                Console.WriteLine("Số lượng đồng xu vàng mà người chơi nhận được là: " + xuDongVang);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"Lỗi: {ex.Message}");
            }
            catch (FormatException)
            {
                Console.WriteLine("Lỗi: Vui lòng nhập một số nguyên.");
            }
        }

        public static int TinhXuDongVang(int mangQuang)
        {
            if (mangQuang < 0)
            {
                throw new ArgumentException("Số mảnh quặng không hợp lệ. Số mảnh quặng phải lớn hơn hoặc bằng 0.");
            }

            int xuDongVang = 0;

            if (mangQuang <= 10)
            {
                xuDongVang = mangQuang * 10;
            }
            else if (mangQuang <= 15)
            {
                xuDongVang = 10 * 10 + (mangQuang - 10) * 5;
            }
            else if (mangQuang <= 18)
            {
                xuDongVang = 10 * 10 + 5 * 5 + (mangQuang - 15) * 2;
            }
            else
            {
                xuDongVang = 10 * 10 + 5 * 5 + 3 * 2 + (mangQuang - 18);
            }
            return xuDongVang;
        }
    }
}
