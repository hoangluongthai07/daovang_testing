﻿using Microsoft.VisualStudio.TestPlatform.TestHost;

namespace EX_ThuThapQuang
{
    public class UnitTest1
    {

        [Fact]
        public void TinhXuDongVang_CalculateCorrectly_With5MangQuang()
        {
            // Arrange
            int mangQuang = 5;
            int expectedXuDongVang = 50;

            // Act
            var actual = Program.TinhXuDongVang(mangQuang);

            // Assert
            Assert.Equal(expectedXuDongVang, actual);
        }

        [Fact]
        public void TinhXuDongVang_CalculateCorrectly_With10MangQuang()
        {
            // Arrange
            int mangQuang = 10;
            int expectedXuDongVang = 100;

            // Act
            var actual = Program.TinhXuDongVang(mangQuang);

            // Assert
            Assert.Equal(expectedXuDongVang, actual);
        }

        [Fact]
        public void TinhXuDongVang_CalculateCorrectly_With20MangQuang()
        {
            // Arrange
            int mangQuang = 20;
            int expectedXuDongVang = 133; 

            // Act
            var actual = Program.TinhXuDongVang(mangQuang);

            // Assert
            Assert.Equal(expectedXuDongVang, actual);
        }

        [Fact]
        public void TinhXuDongVang_InvalidArgument_ThrowsArgumentException()
        {
            // Arrange
            int invalidMangQuang = -5;

            // Act
            Action act = () => Program.TinhXuDongVang(invalidMangQuang);

            // Assert
            var exception = Assert.Throws<ArgumentException>(act);
            Assert.Equal("Số mảnh quặng không hợp lệ. Số mảnh quặng phải lớn hơn hoặc bằng 0.", exception.Message);
        }



    }
}